��          �   %   �      `     a     f     �     �     �     �     �      �     �  $   �     �  %        C  '   H  %   p     �     �     �     �     �  #   �          6  /   K     {     �  �  �  
        '     F     K     Q     W     l      y     �  #   �     �  0   �       +     (   D     m     �  
   �  	   �     �     �     �       @        ]     x                                       
            	                                                                         Arch Could not find command %s Date Device Dir Directory: %s  Device: %s Distro Extra command line parameters %s Label No Linux systems were found under %s Only one Linux system was found Please select a Linux system to visit Quit Rescan all partitions for Linux systems Scan all partitions for Linux systems Scanning directories ... Scanning partitions ... Size Starting %s Strange, %s is not a directory Top directory %s is not a directory Top directory %s not found Unknown parameter %s Use the %s command or %s to return to main menu Visiting distro %s done Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: scootergrisen, 2019
Language-Team: Danish (https://www.transifex.com/anticapitalista/teams/10162/da/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
Plural-Forms: nplurals=2; plural=(n != 1);
 Arkitektur Kunne ikke finde kommandoen %s Dato Enhed Mappe Mappe: %s  Enhed: %s Distribution Ekstra kommandolinjeparametre %s Etiket Fandt ingen Linux-systemer under %s Fandt kun ét Linux-system Vælg venligst et Linux-system som skal besøges Afslut Genskan alle partitioner for Linux-systemer Skal alle partitioner for Linux-systemer Skanner mapper ... Skanner partitioner ... Størrelse Starter%s Underligt, %s er ikke en mappe Topmappen %s er ikke en mappe Topmappen %s blev ikke fundet Ukendt parameter %s Brug %s-kommandoen eller %s for at vende tilbage til hovedmenuen Besøger distributionen %s færdig 