��          t      �                 .     L  !   d     �     �  &   �  '   �     
       �  5     �     �     	  '     !   G     i  ,   �  8   �     �                     	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Taiwanoca <taiwanoca@gmail.com>, 2019
Language-Team: German (https://www.transifex.com/anticapitalista/teams/10162/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 Host %s Datei nicht gefunden! Parameter nach %s erwartet Diese Prozesse töten Ein Verzeichnis ist noch eingehangen %s Zum Beenden bitte auf %s drücken Verdächtiges Argument nach %s Diese Verzeichnisse sind noch eingehangen %s Weiß nicht, wie mit Symlink zu %s verfahren werden soll Host %s gefunden auf %s Ziel %s zeigt auf %s 