��          �   %   �      `     a     f     �     �     �     �     �      �     �  $   �     �  %        C  '   H  %   p     �     �     �     �     �  #   �          6  /   K     {     �  �  �     .  $   3     X     ]     l  #   q     �  4   �  
   �  $   �  )     5   1     g  3   o  1   �  "   �      �             %   1  5   W  2   �     �  <   �  &   	     :	                                       
            	                                                                         Arch Could not find command %s Date Device Dir Directory: %s  Device: %s Distro Extra command line parameters %s Label No Linux systems were found under %s Only one Linux system was found Please select a Linux system to visit Quit Rescan all partitions for Linux systems Scan all partitions for Linux systems Scanning directories ... Scanning partitions ... Size Starting %s Strange, %s is not a directory Top directory %s is not a directory Top directory %s not found Unknown parameter %s Use the %s command or %s to return to main menu Visiting distro %s done Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: d4150ce1d189cce467e7f661b4839a18, 2019
Language-Team: French (https://www.transifex.com/anticapitalista/teams/10162/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
 Arch Impossible de trouver la commande %s Date Périphérique Rép Répertoire: %s  Périphérique: %s Distribution Paramètres de ligne de commande supplémentaires %s Étiquette Aucun système Linux trouvé sous %s Un seul système Linux a été identifié Veuillez choisir un système Linux à passer en revue Quitter Rescanner toutes les partitions des systèmes Linux Scanner toutes les partitions des systèmes Linux Scan des répertoires en cours ... Scan des partitions en cours ... Taille Démarrage de %s Étrange, %s n'est pas un répertoire Le répertoire supérieur %s n'est pas un répertoire Impossible de trouver le répertoire supérieur %s Paramètre Inconnu %s Utilisez la commande %s ou %s pour revenir au menu principal En train d'explorer la distribution %s fait 