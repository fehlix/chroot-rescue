��          �   %   �      `     a     f     �     �     �     �     �      �     �  $   �     �  %        C  '   H  %   p     �     �     �     �     �  #   �          6  /   K     {     �  �  �     L     Y     p     u  
   �     �     �  -   �     �  +   �       (   ;     d  8   j  /   �     �     �            !     )   >      h     �  3   �     �     �                                       
            	                                                                         Arch Could not find command %s Date Device Dir Directory: %s  Device: %s Distro Extra command line parameters %s Label No Linux systems were found under %s Only one Linux system was found Please select a Linux system to visit Quit Rescan all partitions for Linux systems Scan all partitions for Linux systems Scanning directories ... Scanning partitions ... Size Starting %s Strange, %s is not a directory Top directory %s is not a directory Top directory %s not found Unknown parameter %s Use the %s command or %s to return to main menu Visiting distro %s done Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>, 2020
Language-Team: Galician (Spain) (https://www.transifex.com/anticapitalista/teams/10162/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 Arquitectura Comando %s non atopado Data Dispositivo Directorio Directorio: %s  Dispositivo: %s Distribución Parámetros de liña de comando adicionais %s Rótulo Non foi atopado ningún sistema Linux en %s Só se atopou un sistema Linux Selecciona un sistema Linux para visitar Saír Volver a procurar sistemas Linux en todas as particións Procurar sistemas linux en todas as particións Buscando directorios ... Examinando particións ... Tamaño Iniciando %s Estraño, %s non é un directorio Directorio de top %s non é un directorio Directorio de top %s non atopado Parámetro %s descoñecido Usar o comando  ou %s para volver ao menu principal Visitando a distribución %s feito 