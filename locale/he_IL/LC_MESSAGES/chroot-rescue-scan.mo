��          �   %   �      `     a     f     �     �     �     �     �      �     �  $   �     �  %        C  '   H  %   p     �     �     �     �     �  #   �          6  /   K     {     �    �     �  -   �  
   �     �     �             .   (  
   W  3   b  2   �  5   �  
   �  S   
  J   ^     �     �     �     �  $   �  8   #	  0   \	     �	  W   �	     
     
                                       
            	                                                                         Arch Could not find command %s Date Device Dir Directory: %s  Device: %s Distro Extra command line parameters %s Label No Linux systems were found under %s Only one Linux system was found Please select a Linux system to visit Quit Rescan all partitions for Linux systems Scan all partitions for Linux systems Scanning directories ... Scanning partitions ... Size Starting %s Strange, %s is not a directory Top directory %s is not a directory Top directory %s not found Unknown parameter %s Use the %s command or %s to return to main menu Visiting distro %s done Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>, 2021
Language-Team: Hebrew (Israel) (https://www.transifex.com/anticapitalista/teams/10162/he_IL/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: he_IL
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;
 סוג מעבד לא ניתן למצוא את הפקודה %s תאריך התקן תיקייה תיקייה: %s  התקן: %s הפצה משתני שורת פקודה נוספים %s תווית לא נמצאו מערכות לינוקס תחת %s נמצאה מערכת לינוקס אחת בלבד נא לבחור מערכת לינוקס לבקר בה עזיבה לסרוק את כל המחיצות מחדש לאיתור מערכות לינוקס לסרוק את כל המחיצות לאיתור מערכות לינוקס התיקיות נסרקות… המחיצות נסרקות… גודל %s מופעל מוזר, %s איננה תיקייה התיקייה העליונה %s איננה תיקייה התיקייה העליונה %s לא נמצאה משתנה לא ידוע %s יש להשתמש בפקודה %s או ב־%s כדי לחזור לתפריט הראשי ביקור בהפצה %s בוצע 