��          �   %   �      `     a     f     �     �     �     �     �      �     �  $   �     �  %        C  '   H  %   p     �     �     �     �     �  #   �          6  /   K     {     �  �  �     :     ?     W     \     b     h     |      �     �  "   �     �  %   �       1     ,   F     s     �     �  
   �     �     �     �       >        V     i                                       
            	                                                                         Arch Could not find command %s Date Device Dir Directory: %s  Device: %s Distro Extra command line parameters %s Label No Linux systems were found under %s Only one Linux system was found Please select a Linux system to visit Quit Rescan all partitions for Linux systems Scan all partitions for Linux systems Scanning directories ... Scanning partitions ... Size Starting %s Strange, %s is not a directory Top directory %s is not a directory Top directory %s not found Unknown parameter %s Use the %s command or %s to return to main menu Visiting distro %s done Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2020
Language-Team: Norwegian Bokmål (https://www.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 Ark. Fant ikke kommandoen %s Dato Enhet Mappe Mappe: %s Enhet: %s Distro Ekstra kommandolinjeparametre %s Navn Fant ingen Linux-systemer under %s Fant kun ett Linux-system Velg Linux-systemet som skal besøkes Avslutt Nytt søk i alle partisjoner etter Linux-systemer Søk i alle partisjoner etter Linux-systemer Søker i mapper … Søker i partisjoner … Str. Starter %s Merkelig, %s er ikke en mappe Rotmappen %s er ikke en mappe Fant ikke rotmappen %s Ukjent parameter %s Bruk kommandoen %s eller %s for å gå tilbake til hovedmenyen Besøker distro %s ferdig 