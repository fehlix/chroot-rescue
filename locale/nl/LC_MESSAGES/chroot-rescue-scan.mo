��          �   %   �      `     a     f     �     �     �     �     �      �     �  $   �     �  %        C  '   H  %   p     �     �     �     �     �  #   �          6  /   K     {     �  �  �               ;     A     J     N     d  "   k     �  %   �  %   �  1   �       *     '   B     j     }     �  
   �     �     �     �     �  B        O     b                                       
            	                                                                         Arch Could not find command %s Date Device Dir Directory: %s  Device: %s Distro Extra command line parameters %s Label No Linux systems were found under %s Only one Linux system was found Please select a Linux system to visit Quit Rescan all partitions for Linux systems Scan all partitions for Linux systems Scanning directories ... Scanning partitions ... Size Starting %s Strange, %s is not a directory Top directory %s is not a directory Top directory %s not found Unknown parameter %s Use the %s command or %s to return to main menu Visiting distro %s done Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Eadwine Rose, 2019
Language-Team: Dutch (https://www.transifex.com/anticapitalista/teams/10162/nl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl
Plural-Forms: nplurals=2; plural=(n != 1);
 Arch Kon commando %s niet vinden Datum Apparaat Dir Map: %s  Apparaat: %s Distro Extra commando regel parameters %s Label Geen Linux systemen gevonden onder %s Er is maar een Linux systeem gevonden Selecteer a.u.b. een Linux systeem om te bezoeken Stop Herscan alle partities voor Linux systemen Scan alle partities voor Linux systemen Mappen scannen ... Partities scannen ... Grootte Starten %s Vreemd, %s is geen map Hoogste map %s is geen map Hoogste map %s niet gevonden Onbekende parameter %s Gebruik het %s commando of %s om terug te keren naar het hoofdmenu Bezoeken distro %s klaar 