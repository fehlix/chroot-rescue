��          t      �                 .     L  !   d     �     �  &   �  '   �     
       2  5  $   h     �     �  '   �     �       $      7   E     }     �                	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: MX Linux Polska <mxlinuxpl@gmail.com>, 2019
Language-Team: Polish (https://www.transifex.com/anticapitalista/teams/10162/pl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 Nie można znaleźć pliku hosta %s! Oczekiwano parametru po %s Zabijanie tych procesów Jeden katalog jest nadal zamontowany %s Naciśnij %s aby zakończyć Podejrzany argument po %s Te katalogi są nadal zamontowane %s nie wiem, jak obsłużyć dowiązanie symboliczne do %s host %s znaleziony w %s cel %s wskazuje na %s 