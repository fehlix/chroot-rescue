��          t      �                 .     L  !   d     �     �  &   �  '   �     
       �  5  .   �          $  %   ?     e     y  +   �  5   �  %   �                     	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: José Vieira <jvieira33@sapo.pt>, 2020
Language-Team: Portuguese (https://www.transifex.com/anticapitalista/teams/10162/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
 Ficheiro hospedeiro (host) %s não encontrado! Esperado um parâmetro após %s A aniquilar estes pocessos Ainda está montado um directório %s Premir %s para sair Argumento duvidoso após %s Estes directórios ainda estão montados %s utilização do atalho (symlink) para %s desconhecida hospedeiro (host) %s encontrado em %s o destino %s aponta para %s 