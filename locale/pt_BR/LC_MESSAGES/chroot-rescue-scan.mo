��          �   %   �      `     a     f     �     �     �     �     �      �     �  $   �     �  %        C  '   H  %   p     �     �     �     �     �  #   �          6  /   K     {     �  �  �     G      S     t     y  
   �     �     �  -   �     �  .   �  &   $  2   K     ~  9   �  /   �     �     	     '     /  #   <  -   `  %   �     �  4   �     	  
   "	                                       
            	                                                                         Arch Could not find command %s Date Device Dir Directory: %s  Device: %s Distro Extra command line parameters %s Label No Linux systems were found under %s Only one Linux system was found Please select a Linux system to visit Quit Rescan all partitions for Linux systems Scan all partitions for Linux systems Scanning directories ... Scanning partitions ... Size Starting %s Strange, %s is not a directory Top directory %s is not a directory Top directory %s not found Unknown parameter %s Use the %s command or %s to return to main menu Visiting distro %s done Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2020
Language-Team: Portuguese (Brazil) (https://www.transifex.com/anticapitalista/teams/10162/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
 Arquitetura O comando %s não foi encontrado Data Dispositivo Diretório Diretório: %s  Dispositivo: %s Distribuição Parâmetros de linha de comando adicionais %s Rótulo Não foi encontrado nenhum sistema Linux em %s Foi encontrado apenas um sistema Linux Por favor, selecione um sistema Linux para visitar Sair Procurar novamente sistemas Linux em todas as partições Procurar sistemas Linux em todas as partições Pesquisando diretórios ... Procurando nas partições... Tamanho Iniciando %s Estranho, %s  não é um diretório O diretório de topo %s não é um diretório Diretório de topo %s não encontrado Parâmetro %s desconhecido Usar o comando%s ou %s para voltar ao menu principal Visitando a distribuição %s concluído 