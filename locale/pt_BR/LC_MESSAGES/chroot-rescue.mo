��          t      �                 .     L  !   d     �     �  &   �  '   �     
       �  5  <   �     &     F  $   a     �     �  *   �  9   �  %        E                	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2020
Language-Team: Portuguese (Brazil) (https://www.transifex.com/anticapitalista/teams/10162/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
 Não foi possível encontrar o arquivo hospedeiro (host) %s  Esperado um parâmetro após %s Aniquilando estes pocessos Ainda está montado um diretório %s Pressionar %s para sair Argumento duvidoso após %s Estes diretórios ainda estão montados %s desconhecido o tratamento a dar ao atalho/symlink para %s hospedeiro (host) %s encontrado em %s o alvo %s aponta para %s 