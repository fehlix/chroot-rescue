��          �   %   �      `     a     f     �     �     �     �     �      �     �  $   �     �  %        C  '   H  %   p     �     �     �     �     �  #   �          6  /   K     {     �  �  �     k     p     �     �  
   �     �     �  #   �     �  &   �        6   5     l  -   t  %   �     �     �     �               '     B     a  2   u     �  
   �                                       
            	                                                                         Arch Could not find command %s Date Device Dir Directory: %s  Device: %s Distro Extra command line parameters %s Label No Linux systems were found under %s Only one Linux system was found Please select a Linux system to visit Quit Rescan all partitions for Linux systems Scan all partitions for Linux systems Scanning directories ... Scanning partitions ... Size Starting %s Strange, %s is not a directory Top directory %s is not a directory Top directory %s not found Unknown parameter %s Use the %s command or %s to return to main menu Visiting distro %s done Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2020
Language-Team: Slovenian (https://www.transifex.com/anticapitalista/teams/10162/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 Veja Nisem našel ukaza %s Datum Naprava Direktorij Imenik: %s Naprava: %s Distribucija Poseben parameter ukazne vrstice %s Vrsta Pod %s ni bilo najdenih Linux sistemov Najden je bil le en Linux sistem Prosimo, izberite Linux sistem, ki ga želite obiskati Končaj Ponovno preveri vse razdelke za Linux sistemi Preveri vse razdelke za Linux sistemi Iskanje po direktorijih ... Iskanje razdelkov ... Velikost Zaganjam %s Čudno, %s to ni imenik Vrhnji imenik %s ni imenik Vrhnji imenik %s ni bil najden Neznan parameter %s Uporabite ukaz %s ali %s za vrnitev na glavni meni Obisk distribucije %s opravljeno 