��          t      �                 .     L  !   d     �     �  &   �  '   �     
       �  5  "   �     �       '   8  #   `     �  )   �  1   �     �                     	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Besnik Bleta <besnik@programeshqip.org>, 2022
Language-Team: Albanian (https://www.transifex.com/anticapitalista/teams/10162/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 S’u gjet dot kartelë %s strehe! Pritej një parametër pas %s Po asgjësohen këto procese Një drejtori është ende e montuar %s Ju lutemi, shtypni %s që të dilet Argument i dyshimtë pas %s Këto drejtori janë ende të montuara %s s’dihet si të trajtohen lidhje simbolike te %s u gjet strehë %s te %s objektivi %s shpie te %s 