��          �   %   �      `     a     f     �     �     �     �     �      �     �  $   �     �  %        C  '   H  %   p     �     �     �     �     �  #   �          6  /   K     {     �  �  �     /     6     L     R     Y     _  
   t  &        �  -   �      �  4   �     1  ;   7  3   s     �     �     �     �     �          2     L  ;   e      �     �                                       
            	                                                                         Arch Could not find command %s Date Device Dir Directory: %s  Device: %s Distro Extra command line parameters %s Label No Linux systems were found under %s Only one Linux system was found Please select a Linux system to visit Quit Rescan all partitions for Linux systems Scan all partitions for Linux systems Scanning directories ... Scanning partitions ... Size Starting %s Strange, %s is not a directory Top directory %s is not a directory Top directory %s not found Unknown parameter %s Use the %s command or %s to return to main menu Visiting distro %s done Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: e0360fc3c0afcd70e5aed1cfc885bac6, 2019
Language-Team: Turkish (https://www.transifex.com/anticapitalista/teams/10162/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=2; plural=(n > 1);
 Mimari %s komutu bulunamadı Tarih Aygıt Dizin Dizin: %s  Cihaz: %s Dağıtım Ekstra komut satırı parametreleri %s Etiket %s altında hiçbir Linux sistemi bulunamadı Sadece bir Linux sistemi bulundu Lütfen ziyaret etmek için bir Linux sistemi seçin Çık Linux sistemleri için tüm disk-bölümlerini yeniden tara Linux sistemleri için tüm disk-bölümlerini tara Dizinler taranıyor ... Disk Bölümleri taranıyor ... Boyut %s başlatılıyor Tuhaf, %s bir dizin değil Üst dizin %s bir dizin değil Üst dizin %s bulunamadı Bilinmeyen değişken %s Ana menüye dönmek için %s komutunu veya %s 'yi kullanın %s dağıtımı ziyaret ediliyor bitti 